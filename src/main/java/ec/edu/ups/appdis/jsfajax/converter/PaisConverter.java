package ec.edu.ups.appdis.jsfajax.converter;

import java.util.List;

import javax.el.ELContext;
import javax.faces.application.Application;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ec.edu.ups.appdis.jsfajax.Pais;

@FacesConverter("ec.edu.ups.appdis.jsfajax.converter.PaisConverter")
public class PaisConverter implements Converter {
	

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		System.out.println("getAsObject " + value);
		List<Pais> paises= this.getDAO();
		for(Pais c : paises){
			if (c.getCodigo().equals(value))
				return c;
		}
		return null;		
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		System.out.println("getAsString " + value);		
		return ((Pais) value).getCodigo();
	}
	
	public List<Pais> getDAO(){				
		FacesContext ctx = FacesContext.getCurrentInstance();
		ELContext ec = ctx.getELContext();
		Application app = ctx.getApplication();
		return (List) app.evaluateExpressionGet(ctx,"#{localizacion.paises}",List.class);		
	}
	
	
}
