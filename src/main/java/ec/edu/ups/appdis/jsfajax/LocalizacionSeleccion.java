package ec.edu.ups.appdis.jsfajax;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;


@ManagedBean(name="localizacion")
public class LocalizacionSeleccion {
	
	private List<Pais> 			paises;
	private List<Ciudad> 		ciudades;	
	private Pais				pais;
	private Ciudad				ciudad;
	private boolean 		 	isPaisSeleccionado;
	
	@PostConstruct
	public void init(){
		
		paises = new ArrayList<Pais>();
		
		Pais paisAux = new Pais("54", "Argentina");
		paisAux.addCiudad(new Ciudad("01", "Córdoba"));
		paisAux.addCiudad(new Ciudad("02", "Rosario"));
		paisAux.addCiudad(new Ciudad("03", "Mendoza"));
		
		pais = paisAux;
		ciudad = pais.getCiudades().values().iterator().next();
		paises.add(paisAux);
		
		paisAux = new Pais("593", "Ecuador");
		paisAux.addCiudad(new Ciudad("01", "Quito"));
		paisAux.addCiudad(new Ciudad("02", "Cuenca"));
		paisAux.addCiudad(new Ciudad("03", "Guayaquil"));
		
		paises.add(paisAux);
		
		paisAux = new Pais("51", "Perú");
		paisAux.addCiudad(new Ciudad("01", "Lima"));
		paisAux.addCiudad(new Ciudad("02", "Arequipa"));
		paisAux.addCiudad(new Ciudad("03", "Cusco"));
		
		paises.add(paisAux);
	}

	public List<Pais> getPaises() {
		System.out.println("getPaises --> List ");
		return paises;
	}


	public void setPaises(List<Pais> paises) {
		System.out.println("setPaises --> List ");
		this.paises = paises;
	}

	public List<Ciudad> getCiudades() {
		System.out.println("getCiudades --> List ");		
		if (pais==null)
			return null;
		return new ArrayList<Ciudad>(pais.getCiudades().values());
	}
	
	public void setCiudades(List<Ciudad> ciudades) {
		System.out.println("getCiudades --> List ");
		this.ciudades = ciudades;
	}


	public Pais getPais() {
		System.out.println("getPais " + (pais!=null?pais.getNombre():""));
		return pais;
	}


	public void setPais(Pais pais) {
		System.out.println("setPais " + pais.getNombre());
		this.pais = pais;
	}


	public Ciudad getCiudad() {
		System.out.println("getCiudad " + (ciudad!=null?ciudad.getNombre():""));
		return ciudad;
	}


	public void setCiudad(Ciudad ciudad) {
		System.out.println("setCiudad " + ciudad.getNombre());
		this.ciudad = ciudad;
	}


	public boolean isPaisSeleccionado() {
		System.out.println("isPaisSeleccionado " + isPaisSeleccionado);
		return isPaisSeleccionado;
	}


	public void setPaisSeleccionado(boolean isPaisSeleccionado) {
		System.out.println("setPaisSeleccionado " + isPaisSeleccionado);
		this.isPaisSeleccionado = isPaisSeleccionado;
	}
	
	public String doRecargar(){
		System.out.println("doRefrescar");
		return null;
	}

}
