package ec.edu.ups.appdis.jsfajax.converter;

import java.util.HashMap;
import java.util.List;

import javax.el.ELContext;
import javax.faces.application.Application;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ec.edu.ups.appdis.jsfajax.Ciudad;
import ec.edu.ups.appdis.jsfajax.Pais;

@FacesConverter("ec.edu.ups.appdis.jsfajax.converter.CiudadConverter")
public class CiudadConverter implements Converter {
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		System.out.println("getAsObject " + value);
		return this.getDAO().get(value);		
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		System.out.println("getAsString " + value);	
		if (value == null)
			return null;
		return ((Ciudad) value).getCodigo();
	}
	
	public HashMap<String, Ciudad> getDAO(){				
		FacesContext ctx = FacesContext.getCurrentInstance();
		return (HashMap) ctx.getApplication().evaluateExpressionGet(
							ctx,"#{localizacion.pais.ciudades}",HashMap.class);		
	}
	
	
}
