package ec.edu.ups.appdis.jsfajax;

import java.util.LinkedHashMap;

public class Pais {
	
	private String codigo;
	private String nombre;
	private LinkedHashMap<String, Ciudad> ciudades;
	
	
	public Pais(String codigo, String nombre){
		this.codigo = codigo;
		this.nombre = nombre;
		this.ciudades = new LinkedHashMap<String, Ciudad>();
	}
	
	public void addCiudad(Ciudad ciudad){
		ciudades.put(ciudad.getCodigo(), ciudad);		
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public LinkedHashMap<String, Ciudad>getCiudades() {
		return ciudades;
	}
	public void setCiudades(LinkedHashMap<String, Ciudad> ciudades) {
		this.ciudades = ciudades;
	}

}
